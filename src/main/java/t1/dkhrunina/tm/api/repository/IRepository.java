package t1.dkhrunina.tm.api.repository;

import t1.dkhrunina.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @Override
            public Optional<M> findOneById(final String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @Override
            public Optional<M> findOneByIndex(final Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    void clear();

    M add(M model);

    boolean existsById(String id);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    int getSize();

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void removeAll(Collection<M> collection);

    interface IRepositoryOptional<M> {

        Optional<M> findOneById(String id);

        Optional<M> findOneByIndex(Integer index);

    }

}
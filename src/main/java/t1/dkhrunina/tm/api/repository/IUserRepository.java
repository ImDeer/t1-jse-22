package t1.dkhrunina.tm.api.repository;

import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    Boolean isEmailExist(String email);

    Boolean isLoginExist(String login);

    User findByEmail(String email);

    User findById(String id);

    User findByLogin(String login);

}
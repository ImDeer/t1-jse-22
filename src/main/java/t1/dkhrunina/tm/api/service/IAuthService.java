package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.User;

public interface IAuthService {

    void checkRoles(Role[] roles);

    User register(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
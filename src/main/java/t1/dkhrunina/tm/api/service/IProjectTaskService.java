package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String userId, String projectId, String taskId);

    void removeAllByUserId(String userId);

    void removeProjectById(String userId, String projectId);

    Task unbindTaskFromProject(String userId, String projectId, String taskId);

}
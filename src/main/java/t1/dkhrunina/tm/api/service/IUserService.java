package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByEmail(String email);

    User findById(String id);

    User findByLogin(String login);

    Boolean isEmailExist(String email);

    Boolean isLoginExist(String login);

    User removeByEmail(String email);

    User removeByLogin(String login);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
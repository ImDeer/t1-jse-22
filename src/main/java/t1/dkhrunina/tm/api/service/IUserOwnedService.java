package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.api.repository.IUserOwnedRepository;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    List<M> findAll(String userId, Sort sort);

}
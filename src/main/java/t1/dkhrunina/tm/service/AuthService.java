package t1.dkhrunina.tm.service;

import t1.dkhrunina.tm.api.service.IAuthService;
import t1.dkhrunina.tm.api.service.IUserService;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.field.LoginEmptyException;
import t1.dkhrunina.tm.exception.field.PasswordEmptyException;
import t1.dkhrunina.tm.exception.user.AccessDeniedException;
import t1.dkhrunina.tm.exception.user.PermissionException;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

    @Override
    public User register(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final boolean locked = user.isLocked() == null || user.isLocked();
        if (locked) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void lockUserByLogin(final String login) {
        userService.lockUserByLogin(login);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        userService.unlockUserByLogin(login);
    }

}
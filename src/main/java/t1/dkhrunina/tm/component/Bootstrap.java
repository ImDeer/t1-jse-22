package t1.dkhrunina.tm.component;

import t1.dkhrunina.tm.api.repository.ICommandRepository;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.command.AbstractCommand;
import t1.dkhrunina.tm.command.project.*;
import t1.dkhrunina.tm.command.system.*;
import t1.dkhrunina.tm.command.task.*;
import t1.dkhrunina.tm.command.user.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.system.ArgumentNotSupportedException;
import t1.dkhrunina.tm.exception.system.CommandNotSupportedException;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.repository.CommandRepository;
import t1.dkhrunina.tm.repository.ProjectRepository;
import t1.dkhrunina.tm.repository.TaskRepository;
import t1.dkhrunina.tm.repository.UserRepository;
import t1.dkhrunina.tm.service.*;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository, projectTaskService);

    private final IAuthService authService = new AuthService(userService);

    {
        register(new ApplicationAboutCommand());
        register(new ApplicationHelpCommand());
        register(new ApplicationInfoCommand());
        register(new ApplicationVersionCommand());
        register(new ApplicationExitCommand());

        register(new ProjectChangeStatusByIdCommand());
        register(new ProjectChangeStatusByIndexCommand());
        register(new ProjectClearCommand());
        register(new ProjectCompleteByIdCommand());
        register(new ProjectCompleteByIndexCommand());
        register(new ProjectCreateCommand());
        register(new ProjectClearCommand());
        register(new ProjectListCommand());
        register(new ProjectRemoveByIdCommand());
        register(new ProjectRemoveByIndexCommand());
        register(new ProjectShowByIdCommand());
        register(new ProjectShowByIndexCommand());
        register(new ProjectStartByIdCommand());
        register(new ProjectStartByIndexCommand());
        register(new ProjectUpdateByIdCommand());
        register(new ProjectUpdateByIndexCommand());

        register(new TaskBindToProjectCommand());
        register(new TaskChangeStatusByIdCommand());
        register(new TaskChangeStatusByIndexCommand());
        register(new TaskClearCommand());
        register(new TaskCompleteByIdCommand());
        register(new TaskCompleteByIndexCommand());
        register(new TaskCreateCommand());
        register(new TaskListCommand());
        register(new TaskRemoveByIdCommand());
        register(new TaskRemoveByIndexCommand());
        register(new TaskShowByIdCommand());
        register(new TaskShowByIndexCommand());
        register(new TaskShowByProjectIdCommand());
        register(new TaskStartByIdCommand());
        register(new TaskStartByIndexCommand());
        register(new TaskUnbindFromProjectCommand());
        register(new TaskUpdateByIdCommand());
        register(new TaskUpdateByIndexCommand());

        register(new UserLoginCommand());
        register(new UserLogoutCommand());
        register(new UserLockCommand());
        register(new UserRegisterCommand());
        register(new UserRemoveCommand());
        register(new UserChangePasswordCommand());
        register(new UserShowProfileCommand());
        register(new UserUnlockCommand());
        register(new UserUpdateProfileCommand());
    }

    private void initLogger() {
        loggerService.info("*** Welcome to Task Manager ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** Task Manager is shutting down ***");
            }
        });
    }

    private void initDemoData() {
        final User userCat = userService.create("meow", "meow", "meowuser@meow.meow");
        final User userDog = userService.create("woof", "woof");
        final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(userCat.getId(), new Project("Project Quack", Status.COMPLETED));
        projectService.add(userCat.getId(), new Project("Project Baa", Status.IN_PROGRESS));
        projectService.add(userCat.getId(), new Project("Project Woof", Status.IN_PROGRESS));
        projectService.add(userCat.getId(), new Project("Project Meow", Status.NOT_STARTED));

        taskService.add(userCat.getId(), new Task("Woof-Woof"));
        taskService.add(userCat.getId(), new Task("Quack-Quack"));
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length < 1) return;
        processArgument(args[0]);
        exit();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nEnter command: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void exit() {
        System.exit(0);
    }

    private void register(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(final String arg) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public void run(final String... args) {
        processArguments(args);
        initDemoData();
        initLogger();
        processCommands();
    }

}
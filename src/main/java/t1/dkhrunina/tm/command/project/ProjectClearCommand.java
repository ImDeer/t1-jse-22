package t1.dkhrunina.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    private static final String NAME = "pr-clear";

    private static final String DESCRIPTION = "Delete all projects.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Clear project list]");
        getProjectService().clear(getUserId());
    }

}
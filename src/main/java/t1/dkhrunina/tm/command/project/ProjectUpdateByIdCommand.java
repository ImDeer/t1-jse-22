package t1.dkhrunina.tm.command.project;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "pr-update-by-id";

    private static final String DESCRIPTION = "Update project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Update project by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description (optional): ");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().updateById(userId, id, name, description);
    }

}
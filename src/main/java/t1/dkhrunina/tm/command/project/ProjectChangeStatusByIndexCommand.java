package t1.dkhrunina.tm.command.project;

import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    private static final String NAME = "pr-change-status-by-index";

    private static final String DESCRIPTION = "Change project status by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Change project status by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status: ");
        System.out.println(Arrays.toString(Status.toValues()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, status);
    }

}
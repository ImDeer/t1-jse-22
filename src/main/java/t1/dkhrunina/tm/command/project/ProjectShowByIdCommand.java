package t1.dkhrunina.tm.command.project;

import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "pr-show-by-id";

    private static final String DESCRIPTION = "Show project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Show project by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

}
package t1.dkhrunina.tm.command.project;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    private static final String NAME = "pr-create";

    private static final String DESCRIPTION = "Create new project.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Create project]");
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

}
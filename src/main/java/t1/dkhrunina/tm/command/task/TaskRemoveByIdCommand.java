package t1.dkhrunina.tm.command.task;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "t-remove-by-id";

    private static final String DESCRIPTION = "Remove task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Remove task by id]");
        System.out.println("Enter id: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().removeById(userId, id);
    }

}
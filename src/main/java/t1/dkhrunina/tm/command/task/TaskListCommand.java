package t1.dkhrunina.tm.command.task;

import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    private static final String NAME = "t-list";

    private static final String DESCRIPTION = "Show task list.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Task list]");
        System.out.println("Enter sort type (optional): ");
        System.out.println(Arrays.toString(Sort.toValues()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
    }

}
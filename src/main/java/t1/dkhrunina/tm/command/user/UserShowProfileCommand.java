package t1.dkhrunina.tm.command.user;

import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    private static final String NAME = "u-show-profile";

    private static final String DESCRIPTION = "Show current user profile.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[Show user profile]");
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Email: " + user.getEmail());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
package t1.dkhrunina.tm.command.user;

import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    private static final String NAME = "u-unlock";

    private static final String DESCRIPTION = "Unlock user by login .";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[Unlock user]");
        System.out.println("Enter login: ");
        final String login = TerminalUtil.nextLine();
        getAuthService().unlockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
package t1.dkhrunina.tm.command.system;

import t1.dkhrunina.tm.api.service.ICommandService;
import t1.dkhrunina.tm.command.AbstractCommand;
import t1.dkhrunina.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
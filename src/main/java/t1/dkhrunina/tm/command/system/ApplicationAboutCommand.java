package t1.dkhrunina.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private static final String ARGUMENT = "-a";

    private static final String NAME = "about";

    private static final String DESCRIPTION = "Show developer info.";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("\n[ABOUT]");
        System.out.println("Name: Daria Khrunina");
        System.out.println("Email: dkhrunina@t1-consulting.ru");
    }

}